#!/bin/sh
#
# autogen.sh
#

PKG_NAME="crimson"

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

# check the version of a package
check_version() {
  COMMAND=$1
  MAJOR=$2
  MINOR=$3
  MICRO=$4

  ($COMMAND --version) </dev/null >/dev/null 2>&1 || return 1

  ver=`$COMMAND --version|head -n 1|sed 's/^.*) //'|sed 's/ (.*)//'`
  major=`echo $ver | cut -d. -f1 | sed s/[a-zA-Z\-].*//g`
  minor=`echo $ver | cut -d. -f2 | sed s/[a-zA-Z\-].*//g`
  micro=`echo $ver | cut -d. -f3 | sed s/[a-zA-Z\-].*//g`
  test -z "$major" && major=0
  test -z "$minor" && minor=0
  test -z "$micro" && micro=0

  fail=0
  if [ ! "$major" -gt "$MAJOR" ]; then
    if [ "$major" -lt "$MAJOR" ]; then
      fail=1
    elif [ ! "$minor" -gt "$MINOR" ]; then
      if [ "$minor" -lt "$MINOR" ]; then
        fail=1
      elif [ "$micro" -lt "$MICRO" ]; then
        fail=1
      fi
    fi
  fi

  return "$fail"
}


echo "Auto-regenerate package \`$PKG_NAME' in directory \`$srcdir'";

AUTOMAKE=
ACLOCAL=
DIE=0

(test -f $srcdir/configure.ac \
  && test -f $srcdir/src/cf/main.cpp) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level $PKG_NAME directory"
    DIE=1
}

(autoconf --version) </dev/null >/dev/null 2>&1 || {
  echo
  echo "**Error**: You must have \`autoconf' installed to compile $PKG_NAME."
  echo "Download the appropriate package for your distribution,"
  echo "or get the source tarball at ftp://ftp.gnu.org/pub/gnu/"
  DIE=1
}

for AM in automake-1.10 automake-1.9 automake-1.8 automake-1.7 automake; do
  (check_version $AM 1 7 0) && {
    AUTOMAKE=$AM
    break
  }
done

if [ -z $AUTOMAKE ]; then
  echo
  echo "**Error**: You must have \`automake' version 1.7.0 or greater"
  echo "installed to compile $PKG_NAME. Download the appropriate package"
  echo "for your distribution, or get the source tarball at"
  echo "ftp://ftp.gnu.org/pub/gnu/"
  DIE=1
fi

for ACL in aclocal-1.10 aclocal-1.9 aclocal-1.8 aclocal-1.7 aclocal; do
  (check_version $ACL 1 7 0) && {
    ACLOCAL=$ACL
    break
  }
done

if [ -z $ACLOCAL ]; then
  echo
  echo "**Error**: Missing \`aclocal'. The version of \`automake'"
  echo "installed seems to be outdated. Get a recent package from"
  echo "ftp://ftp.gnu.org/pub/gnu/"
  DIE=1
fi

if test "$DIE" -ne 0; then
  exit 1
fi

#
# actually do something
#
if test -z "$*"; then
  echo "**Warning**: I am going to run \`configure' with no arguments."
  echo "If you wish to pass any, please specify them on the"
  echo \`$0\'" command line."
  echo
fi

echo "Generating configuration files for $PKG_NAME, please wait..."
echo

case $CC in
xlc)
  am_opt=--include-deps;;
esac

macrodirs=`sed -n -e 's,AM_ACLOCAL_INCLUDE(\(.*\)),\1,gp' <$srcdir/configure.ac`
pwd=`pwd`
cd $srcdir

echo "Creating config sub-directory..."
mkdir -p config

echo "Running $ACLOCAL..."
$ACLOCAL $ACLOCAL_FLAGS

if grep "^AM_CONFIG_HEADER" configure.ac >/dev/null; then
	echo "Running autoheader..."
	autoheader
fi

echo "Running $AUTOMAKE --add-missing $am_opt..."
$AUTOMAKE --add-missing $am_opt

echo "Running autoconf..."
autoconf

cd $pwd

#
# configure
#
#conf_flags="--enable-maintainer-mode"

if test x$NOCONFIGURE = x; then
  echo "************"
  echo "Running $srcdir/configure $conf_flags $@..."
  echo "************"

  $srcdir/configure $conf_flags "$@" || exit 1
else
  echo "Skipping configure process"
fi

echo "You can now type 'make' to build $PKG_NAME"

